package com.ravenpack.alphabetsoup;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.apache.commons.lang3.StringUtils.EMPTY;

public class AlphabetSoupServiceImplTest {
    public static final String SOME_MESSAGE_TO_COMPOSE = "SOME MESSAGE TO COMPOSE";
    public static final String SOME_BOWL_CONTENT = "SOME BOWL CONTENT";

    private AlphabetSoupServiceImpl alphabetSoupServiceImpl;

    @Before
    public void setUp() throws Exception {
        alphabetSoupServiceImpl = new AlphabetSoupServiceImpl();
    }

    @Test
    public void emptyMessageCanAlwaysBeComposed() throws Exception {
        Assert.assertTrue(alphabetSoupServiceImpl.isMessageComposableWithBowlContent(EMPTY, SOME_BOWL_CONTENT));
        Assert.assertTrue(alphabetSoupServiceImpl.isMessageComposableWithBowlContent(EMPTY, EMPTY));
        Assert.assertTrue(alphabetSoupServiceImpl.isMessageComposableWithBowlContent("   ", SOME_BOWL_CONTENT));
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullValueForMessageThrowsAnException() throws Exception {
        alphabetSoupServiceImpl.isMessageComposableWithBowlContent(null, SOME_BOWL_CONTENT);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullValueForBowlSoupThrowsAnException() throws Exception {
        alphabetSoupServiceImpl.isMessageComposableWithBowlContent(SOME_MESSAGE_TO_COMPOSE, null);
    }

    @Test
    public void whenTheMessageCanNotBeComposedWithBowlContentThenFalseIsReturned() throws Exception {
        Assert.assertFalse(alphabetSoupServiceImpl.isMessageComposableWithBowlContent("COMPOSE THIS MESSAGE", "NOT MATCHING BOWL CONTENT"));
    }

    @Test
    public void whenTheStringCanBeComposedWithBowlContentThenTrueIsReturned() throws Exception {
        Assert.assertTrue(alphabetSoupServiceImpl.isMessageComposableWithBowlContent("COMPOSE THIS MESSAGE", "MATCH THIS COMPOSE MESSAGE"));
    }
}

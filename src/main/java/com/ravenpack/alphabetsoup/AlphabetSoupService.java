package com.ravenpack.alphabetsoup;

/**
 * Service supporting AlphabetSoup operations
 */
public interface AlphabetSoupService {

    /**
     * Determines if a String can be composed with the characters contained in a particular alphabetSoup.
     *
     * @param stringToCheck the String that needs to be checked against the Alphabet Soup
     * @param alphabetSoup  the String representing the Alphabet Soup
     * @return true if the String can be fully composed with Characters in the Alphabet Soup. returns false otherwise
     * @throws IllegalArgumentException if null is passed to stringToCheck or alphabetSoup
     */
    boolean isMessageComposableWithBowlContent(String stringToCheck, String alphabetSoup);

}

package com.ravenpack.alphabetsoup;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.*;

public class AlphabetSoupServiceImpl implements AlphabetSoupService {

    /**
     * This function takes O(n+m) to run.
     * Where n is length of message and m the the length of bowlContent.
     */
    @Override
    public boolean isMessageComposableWithBowlContent(String message, String bowlContent) {
        if (message == null || bowlContent == null) {
            throw new IllegalArgumentException();
        }
        message = StringUtils.deleteWhitespace(message);
        if (StringUtils.isEmpty(message)) {
            return true;
        }

        Map<Character, Long> messageCharactersCountMap = extractCountOfCharacters(message);

        for (int bowlCharIndex = 0; bowlCharIndex < bowlContent.length(); bowlCharIndex++) {
            reduceCharacterCountInMap(messageCharactersCountMap, bowlContent.charAt(bowlCharIndex));
            if (messageCharactersCountMap.isEmpty()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Reduce the character count in the Map or removes the character if it's the last one in the map
     *
     * @param charactersCountMap the Map to remove the character form
     * @param character                the character that needs to be removed
     */
    private void reduceCharacterCountInMap(Map<Character, Long> charactersCountMap, char character) {
        Long characterCount = charactersCountMap.get(character);
        if (characterCount == null) {
            return;
        }

        characterCount--;
        if (characterCount == 0) {
            charactersCountMap.remove(character);
            return;
        }
        charactersCountMap.put(character, characterCount);
    }

    /**
     * Extracts from a string a Map containing as a key a specific character and as Value the count (number of times) the character appears in the String
     *
     * @param string the string to extract the characters from
     * @return the extracted Character count Map
     */
    private Map<Character, Long> extractCountOfCharacters(String string) {
        return string
                .chars()
                .mapToObj(e -> (char) e)
                .collect(toList())
                .stream()
                .collect(groupingBy(Function.identity(), counting()));
    }

}
